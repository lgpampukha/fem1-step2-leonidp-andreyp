// -------------кнопка создания формы визита------------
let visitBtn = document.querySelector('.visit-btn');

visitBtn.addEventListener('click', function () {
    let form = document.querySelector('.wrapper-form');
    form.style.display = 'block';

    let allInput = document.querySelectorAll('.general');
    allInput.forEach(function (e) {
        e.required = true
    });
});

// -----------------основная форма------------

let wrapperForm = document.createElement('div');
wrapperForm.className = 'wrapper-form';
document.body.appendChild(wrapperForm);

// --------------форма доктора+инпуты-------------

let visitInfoForm = document.createElement('form');
visitInfoForm.className = 'visit-info';
wrapperForm.appendChild(visitInfoForm);


// ---------------доктора лист--------------

let selectList = document.createElement('select');
selectList.className = 'select-doctor';
visitInfoForm.appendChild(selectList);

// ----------------title div------------------

let titleWrapp = document.createElement('div');
titleWrapp.className = 'titleWrapp';

let chooseDoctor = document.createElement('p');
titleWrapp.appendChild(chooseDoctor);
chooseDoctor.className = 'title';
chooseDoctor.innerHTML = 'Выберите доктора!';

let closeBtn = document.createElement('span');
closeBtn.className = 'closeBtn';
titleWrapp.appendChild(closeBtn);
closeBtn.innerHTML = 'X';

visitInfoForm.insertBefore(titleWrapp, selectList);

// ---------------доктора--------------

let optionCardio = document.createElement('option');
optionCardio.className = 'doctorName';
selectList.appendChild(optionCardio);
optionCardio.innerHTML = 'Кардиолог';
optionCardio.value = 'Cardiologist';

let optionDantist = document.createElement('option');
optionDantist.className = 'doctorName';
selectList.appendChild(optionDantist);
optionDantist.innerHTML = 'Стоматолог';
optionDantist.value = 'Dentist';

let optionTherapist = document.createElement('option');
optionTherapist.className = 'doctorName';
selectList.appendChild(optionTherapist);
optionTherapist.innerHTML = 'Терапевт';
optionTherapist.value = 'Therapist';

// ------------------общие инпуты--------------------

let userName = document.createElement('input');
userName.className = 'general';
userName.placeholder = 'ФИО';
userName.id = 'userNameId';
userName.name = 'user';
userName.type = 'text';


let visitGoal = document.createElement('input');
visitGoal.className = 'general';
visitGoal.placeholder = 'Цель визита';
visitGoal.name = 'visit';

let userAge = document.createElement('input');
userAge.className = 'general';
userAge.placeholder = 'Возраст';


let dateOfVisit = document.createElement('input');
dateOfVisit.className = 'general';
dateOfVisit.placeholder = 'Дата визита';
dateOfVisit.id = 'dateOfVisit';
dateOfVisit.name = 'dateof';


// ---------------кардио инпуты------------------

let generalPressure = document.createElement('input');
generalPressure.className = 'general';
generalPressure.placeholder = 'Давление';


let bodyWeight = document.createElement('input');
bodyWeight.className = 'general';
bodyWeight.placeholder = 'Вес';

let heartDesiase = document.createElement('input');
heartDesiase.className = 'general';
heartDesiase.placeholder = 'Заболевания сердца';

// -----------------стоматолог------------/

let lastVisit = document.createElement('input');
lastVisit.className = 'general';
lastVisit.placeholder = 'Дата последнего визита';
lastVisit.name = 'last';


// -----------------create visit Btn-------------

let creatBtn = document.createElement('input');
creatBtn.className = 'create-btn';
creatBtn.innerHTML = 'Create new Visit';
creatBtn.type = 'submit';
creatBtn.name = 'btn';

selectList.onchange = function () {
    let doctors = selectList.value;
    removeInput();
    if (doctors === 'Dentist') {
        visitInfoForm.appendChild(userName);
        visitInfoForm.appendChild(dateOfVisit);
        visitInfoForm.appendChild(visitGoal);
        visitInfoForm.appendChild(lastVisit);
        visitInfoForm.appendChild(textArea);
        visitInfoForm.appendChild(creatBtn);
    }
    if (doctors === 'Cardiologist') {
        visitInfoForm.appendChild(userName);
        visitInfoForm.appendChild(dateOfVisit);
        visitInfoForm.appendChild(visitGoal);
        visitInfoForm.appendChild(generalPressure);
        visitInfoForm.appendChild(bodyWeight);
        visitInfoForm.appendChild(heartDesiase);
        visitInfoForm.appendChild(userAge);
        visitInfoForm.appendChild(textArea);
        visitInfoForm.appendChild(creatBtn);
    }
    if (doctors === 'Therapist') {
        visitInfoForm.appendChild(userName);
        visitInfoForm.appendChild(dateOfVisit);
        visitInfoForm.appendChild(visitGoal);
        visitInfoForm.appendChild(userAge);
        visitInfoForm.appendChild(textArea);
        visitInfoForm.appendChild(creatBtn)
    }
};

// ----------------input comment---------

let textArea = document.createElement('textarea');
textArea.className = 'text-area general';
textArea.maxLength = '400';

// ---------убираем инпуты-----------

function removeInput() {
    userAge.remove();
    visitGoal.remove();
    lastVisit.remove();
    userName.remove();
    generalPressure.remove();
    bodyWeight.remove();
    heartDesiase.remove();
    textArea.remove();
    creatBtn.remove();
}


// -------------дефолт инпуты----------
function defaultInput() {
    visitInfoForm.appendChild(userName);
    visitInfoForm.appendChild(dateOfVisit);
    visitInfoForm.appendChild(visitGoal);
    visitInfoForm.appendChild(generalPressure);
    visitInfoForm.appendChild(bodyWeight);
    visitInfoForm.appendChild(heartDesiase);
    visitInfoForm.appendChild(userAge);
    visitInfoForm.appendChild(textArea);
    visitInfoForm.appendChild(creatBtn);
}

defaultInput();


// --------------------кнопка Х---------------

closeBtn.addEventListener('click', function () {
    wrapperForm.style.display = 'none';
    document.querySelectorAll('.general').forEach(function (elem) {
        elem.value = null;
    });
});

//-------------------Получаем эллементы--------------
let titleName = document.getElementById('userNameId');
let dateVisit = document.getElementById('dateOfVisit');
let noItem = document.querySelector('.no-item');

// --------------Закрываем модалку кликая вне формы---------

wrapperForm.addEventListener('click', function (event) {
    if (event.currentTarget === event.target) {
        wrapperForm.style.display = 'none'
    }
});

let arr = [];
visitInfoForm.onsubmit = function () {
    let doctor = selectList.value;
    let newVisit;

    if (doctor === 'Dentist') {
        newVisit = new VisitDentist(titleName.value, selectList.value, dateVisit.value,
            visitGoal.value, lastVisit.value, textArea.value);
    }

    else if (doctor === 'Therapist') {
        newVisit = new VisitTherapist(titleName.value, selectList.value, dateVisit.value, visitGoal.value, userAge.value, textArea.value);
    }
    else if (doctor === 'Cardiologist') {
        newVisit = new VisitCardio(titleName.value, selectList.value, dateVisit.value, visitGoal.value, generalPressure.value, bodyWeight.value, heartDesiase.value, userAge.value, textArea.value);
    }
    arr.push(newVisit);
    localStorage.setItem('card', JSON.stringify(arr));
    console.log(arr);
    newVisit.creatVisitorCard();
    document.querySelectorAll('.general').forEach(function (elem) {
        elem.value = null;
    });
    return false
};


// -------------классы-------------

class Visit {
    constructor(titleName, selectList, dateVisit, textArea) {
        this.userName = titleName;
        this.dateOfVisit = dateVisit;
        this.nameOfVisit = selectList;
        this.comments = textArea;
    }

    creatVisitorCard() {
        let visitorCard = document.createElement('div');
        visitorCard.className = 'visitor-card';
        wrapperForm.style.display = 'none';

        let board = document.querySelector('.board');
        board.appendChild(visitorCard);

        let closeCard = document.createElement('span');
        closeCard.className = 'close-card';
        closeCard.innerHTML = 'x';
        visitorCard.appendChild(closeCard);

        let userName = document.createElement('p');
        visitorCard.appendChild(userName);
        userName.innerHTML = 'ФИО:' + '' + this.userName;

        let doctorName = document.createElement('p');
        visitorCard.appendChild(doctorName);
        doctorName.innerHTML = 'Доктор:' + '' + this.nameOfVisit;

        let showMore = document.createElement('p');
        showMore.style.display = 'inline-block';
        showMore.className = 'show-more';
        showMore.innerHTML = 'Показать больше';
        visitorCard.appendChild(showMore);

        let rollUpBtn = document.createElement('p');
        rollUpBtn.innerHTML = 'Свернуть';
        rollUpBtn.className = 'show-more';

        noItem.style.display = 'none';

        // --------------Кнопка показать больше-----------

        showMore.addEventListener('click', function (event) {
            let divCards = document.querySelectorAll('.visitor-card');
            let currentIndex = 0;
            divCards.forEach(function (elem, index) {
                if (event.target.parentNode === elem) {
                    currentIndex = index;
                }
            });

            for (let key in arr[currentIndex]) {
                if (key !== 'nameOfVisit' && key !== 'userName') {
                    let pDateVisit = document.createElement('p');
                    pDateVisit.className = 'show-more-info';
                    let pTargetVisit = document.createElement('p');
                    pTargetVisit.className = 'show-more-info';
                    let pPressure = document.createElement('p');
                    pPressure.className = 'show-more-info';
                    let pWeight = document.createElement('p');
                    pWeight.className = 'show-more-info';
                    let pDiseases = document.createElement('p');
                    pDiseases.className = 'show-more-info';
                    let pAge = document.createElement('p');
                    pAge.className = 'show-more-info';
                    let dateLastVisit = document.createElement('p');
                    dateLastVisit.className = 'show-more-info';
                    let pTextArea = document.createElement('p')
                    pTextArea.className = 'show-more-info';
                    console.log(key);

                    switch (key) {
                        case 'dateOfVisit':
                            pDateVisit.innerHTML = `Дата визита: ${arr[currentIndex][key]}`;
                            break;
                        case 'visitGoal':
                            pDateVisit.innerHTML = `Цель визита: ${arr[currentIndex][key]}`;
                            break;
                        case 'pressure':
                            pPressure.innerHTML = `Давление: ${arr[currentIndex][key]}`;
                            break;
                        case 'weight':
                            pWeight.innerHTML = `Вес: ${arr[currentIndex][key]}`;
                            break;
                        case 'diseases':
                            pDiseases.innerHTML = `Заболевания сердца: ${arr[currentIndex][key]}`;
                            break;
                        case 'userAge':
                            pAge.innerHTML = `Возраст: ${arr[currentIndex][key]}`;
                            break;
                        case 'lastVisit':
                            dateLastVisit.innerHTML = `Дата последнего визита: ${arr[currentIndex][key]}`;
                        case 'comments':
                            pTextArea.innerHTML = `Ваши комментарии: ${arr[currentIndex][key]}`;

                            break;
                    }
                    visitorCard.insertBefore(pDateVisit, showMore);
                    visitorCard.insertBefore(pTargetVisit, showMore);
                    visitorCard.insertBefore(pPressure, showMore);
                    visitorCard.insertBefore(pWeight, showMore);
                    visitorCard.insertBefore(pDiseases, showMore);
                    visitorCard.insertBefore(pAge, showMore);
                    visitorCard.insertBefore(dateLastVisit, showMore);
                    visitorCard.insertBefore(pTextArea, showMore);
                    showMore.style.display = 'none';
                }
            }
            visitorCard.appendChild(rollUpBtn);
            rollUpBtn.addEventListener('click', function () {
                visitorCard.querySelectorAll('.show-more-info').forEach(function (elem) {
                    elem.remove()
                });
                rollUpBtn.remove();
                showMore.style.display = 'block'
            })
        });

        // ------------------------Закрыть карточку----------------
        closeCard.addEventListener('click', function (event) {
            let divCards = document.querySelectorAll('.visitor-card');
            let curIndex = 0;
            divCards.forEach(function (elem, index) {
                if (event.target.parentNode === elem)
                    curIndex = index;
            });
            arr.splice(curIndex, 1);
            console.log(arr);
            visitorCard.remove();
            localStorage.setItem('card', JSON.stringify(arr));

            if (arr.length === 0) {
                noItem.style.display = 'block'
            }

        });

// ----------DRAG&DROP--------------

        let startCoorY, startCoorX, newPosX = 0, newPosY = 0, dragStatus = false;
        let minCoordinateX = -visitorCard.offsetLeft;
        let minCoordinateY = -visitorCard.offsetTop;

        let maxCoordinateX = board.offsetWidth + minCoordinateX - 150;
        console.log(maxCoordinateX);
        let maxCoordinateY = board.offsetHeight + minCoordinateY - visitorCard.offsetHeight;
        console.log(maxCoordinateY);


        visitorCard.addEventListener('mousedown', function (e) {
            dragStatus = true;
            startCoorX = e.pageX;
            startCoorY = e.pageY;
            // console.log(startCoorX);
            maxCoordinateY = board.offsetHeight + minCoordinateY - visitorCard.offsetHeight;
            visitorCard.style.left = e.pageX - startCoorX + newPosX + 'px';
            visitorCard.style.top = e.pageY - startCoorY + newPosY + 'px';
            visitorCard.style.zIndex = '2';

        });

        document.addEventListener('mousemove', function (e) {
            let cursorPosX = e.pageX - startCoorX + newPosX
            let cursorPosY = e.pageY - startCoorY + newPosY
            if (dragStatus) {
                if (cursorPosX > minCoordinateX && cursorPosY > minCoordinateY && cursorPosX < maxCoordinateX && cursorPosY < maxCoordinateY) {
                    visitorCard.style.left = e.pageX - startCoorX + newPosX + 'px';
                    visitorCard.style.top = e.pageY - startCoorY + newPosY + 'px';
                }
            }
        });


        document.addEventListener('mouseup', function () {
            if (dragStatus) {
                dragStatus = false;
                newPosX = parseInt(visitorCard.style.left);
                newPosY = parseInt(visitorCard.style.top);
                // console.log(newPosX);
            }
        });

        visitorCard.ondragstart = function () {
            return false
        };
    }

}

class VisitCardio extends Visit {
    constructor(titleName, selectList, dateVisit, visitGoal, generalPressure, bodyWeight, heartDesiase, userAge, textArea) {
        super(titleName, selectList, dateVisit, textArea);
        this.visitGoal = visitGoal;
        this.pressure = generalPressure;
        this.weight = bodyWeight;
        this.diseases = heartDesiase;
        this.userAge = userAge;
    }
}

class VisitDentist extends Visit {
    constructor(titleName, selectList, dateVisit, visitGoal, lastVisit, textArea) {
        super(titleName, selectList, dateVisit, textArea);
        this.visitGoal = visitGoal;
        this.lastVisit = lastVisit;
    }
}

class VisitTherapist extends Visit {
    constructor(titleName, selectList, dateVisit, visitGoal, userAge, textArea) {
        super(titleName, selectList, dateVisit, textArea);
        this.userAge = userAge;
        this.visitGoal = visitGoal;
    }
}

if (localStorage.getItem('card')) {
    arr = JSON.parse(localStorage.getItem('card'));
    arr.forEach(function (card) {
        card.__proto__ = Visit.prototype;
        card.creatVisitorCard()
    })
}





